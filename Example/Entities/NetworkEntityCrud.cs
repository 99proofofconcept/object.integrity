﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using CoreFx.Validations;
using DataTransferObjects.Network;
using DataTransferObjects.Network.Assertions;
using DataTransferObjects.Network.Validations;

namespace DummySampleInUse.Entities
{
    internal class NetworkEntityCrud: BaseNetworkEntityCrud, IInjectValidation<INetworkValidation>
    {
        private Warnings warnings;

        internal NetworkEntityCrud()
        {
            Advices = new List<string>();
        }

        INetworkValidation IInjectValidation<INetworkValidation>.SourceValidation { get; set; }
        
        protected override void HandleErrorsOfNetworkAddress()
        {
            StringEnumerator enumerator = warnings.Lines.GetEnumerator();

            while (enumerator.MoveNext())
            {
                Advices.Add($"Error in { enumerator.Current } property ");
            }
  
            HandleWarnings();
        }

        protected override bool? Validate(NetworkAddress networkAddress)
        {
            if (((IInjectValidation<INetworkValidation>)this).SourceValidation != null)
            {
                warnings = new Warnings();

                return networkAddress
                      .BuildIntegrity(((IInjectValidation<INetworkValidation>)this).SourceValidation, warnings)
                      .EvaluateIntersect();
            }
            
            return null;
        }

        protected bool WasItValidated(bool? isNetworkValid) 
            => (isNetworkValid.HasValue && !isNetworkValid.Value);

        
        public override void Insert(NetworkAddress networkAddress)
        {
            bool? validated = Validate(networkAddress);

            if (WasItValidated(validated))
                HandleErrorsOfNetworkAddress();
            else
                throw new NotImplementedException("Missing logic to insert entity");
        }

        public override void Delete(NetworkAddress networkAddress)
        {
            throw new NotImplementedException();
        }

        public override void Update(NetworkAddress networkAddress)
        {
            throw new NotImplementedException();
        }

    }
}
