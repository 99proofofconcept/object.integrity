﻿using System.Collections.Generic;
using CoreFx.Notifications;
using DataTransferObjects.Network;

namespace DummySampleInUse.Entities
{
    internal abstract class BaseNetworkEntityCrud
    {
        public event Handlers.NotificationsHandler<NotificationsEventArgs> Notifications;

        protected List<string> Advices;

        protected void HandleWarnings()
        {
           Notifications?.Invoke(this, new NotificationsEventArgs(Advices));
        }

        public abstract void Insert(NetworkAddress networkAddress);

        public abstract void Delete(NetworkAddress networkAddress);

        public abstract void Update(NetworkAddress networkAddress);

        protected abstract bool? Validate(NetworkAddress networkAddress);
        
        protected abstract void HandleErrorsOfNetworkAddress();

    }
}