﻿using System.Linq;
using DataTransferObjects.Network;
using DataTransferObjects.Network.Assertions;
using DataTransferObjects.Network.Validations;
using FakeItEasy;
using Xunit;

namespace Test
{
    public class IntegrityStateObjectTest
    {
        private INetworkValidation Fake(bool validIp, bool validMac, bool validSerie)
        {
            var fakeNetworkValidation = A.Fake<INetworkValidation>();

            A.CallTo(() => fakeNetworkValidation.IsValidIpAddress(A<string>.Ignored)).Returns(validIp);
            A.CallTo(() => fakeNetworkValidation.IsValidMacAddress(A<string>.Ignored)).Returns(validMac);
            A.CallTo(() => fakeNetworkValidation.IsValidSerieAddress(A<string>.Ignored)).Returns(validSerie);

            return fakeNetworkValidation;
        }


        [Fact]
        public void Should_pass_the_integrity()
        {
            var fakeNetworkValidation = Fake(validIp: true, validMac: true, validSerie: true);  
            
            NetworkAddress networkAddress = new NetworkAddress();
            Warnings warnings = new Warnings();

            var integrity = networkAddress
                            .BuildIntegrity(fakeNetworkValidation, warnings)
                            .EvaluateAll();

            Assert.True(integrity);
            Assert.Empty(warnings.Lines);
        }

        [Fact]
        public void Should_not_pass_the_integrity_because_wrong_ip()
        {
            var fakeNetworkValidation = Fake(validIp: false, validMac: true, validSerie: true);
            
            NetworkAddress networkAddress = new NetworkAddress();
            Warnings warnings = new Warnings();

            var integrity = networkAddress
                            .BuildIntegrity(fakeNetworkValidation, warnings)
                            .EvaluateAll();
            
            Assert.False(integrity);
            Assert.Single(warnings.Lines);
            Assert.Equal("Ip", warnings.Lines[0]);
        }

        [Fact]
        public void Should_not_pass_the_integrity_because_wrong_mac()
        {
            var fakeNetworkValidation = Fake(validIp: true, validMac: false, validSerie: true);

            NetworkAddress networkAddress = new NetworkAddress();
            Warnings warnings = new Warnings();

            var integrity = networkAddress
                            .BuildIntegrity(fakeNetworkValidation, warnings)
                            .EvaluateAll();
            
            Assert.False(integrity);
            Assert.Single(warnings.Lines);
            Assert.Equal("Mac", warnings.Lines[0]);
        }

        [Fact]
        public void Should_not_pass_the_integrity_because_wrong_serie()
        {
            var fakeNetworkValidation = Fake(validIp: true, validMac: true, validSerie: false);

            NetworkAddress networkAddress = new NetworkAddress();
            Warnings warnings = new Warnings();

            var integrity = networkAddress
                           .BuildIntegrity(fakeNetworkValidation, warnings)
                           .EvaluateAll();
            
            Assert.False(integrity);
            Assert.Single(warnings.Lines);
            Assert.Equal("Serie", warnings.Lines[0]);
        }

        [Fact]
        public void Should_not_pass_the_integrity_because_all_properties_wrong()
        {
            var fakeNetworkValidation = Fake(validIp: false, validMac: false, validSerie: false);

            NetworkAddress networkAddress = new NetworkAddress();
            Warnings warnings = new Warnings();

            var integrity = networkAddress
                            .BuildIntegrity(fakeNetworkValidation, warnings)
                            .EvaluateIntersect();

           
            Assert.False(integrity);
            Assert.Equal(3, warnings.Lines.Count);
            Assert.Equal("Ip", warnings.Lines[0]);
            Assert.Equal("Mac", warnings.Lines[1]);
            Assert.Equal("Serie", warnings.Lines[2]);
        }
    }
}
