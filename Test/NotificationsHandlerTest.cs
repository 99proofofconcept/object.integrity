﻿using System.Collections.Generic;
using CoreFx.Notifications;
using Xunit;

namespace Test
{

    public class NotificationsHandlerTest
    {
        [Fact]
        public void Should_notify_about_warnings()
        {
            Dummy dummy = new Dummy();
           
            List<string> warnings = default(List<string>);
            object source = null;

            dummy.Notifications += (object sender, NotificationsEventArgs e) =>
            {
                warnings = e.Messeges;
                source = sender;
            };

            dummy.DummyOperation();

            Assert.Equal(2, warnings.Count);
            Assert.Contains("First warning", warnings);
            Assert.Contains("Second warning", warnings);

            Assert.IsAssignableFrom<Dummy>(source);

        }
    }


    internal class Dummy
    {
        internal event Handlers.NotificationsHandler<NotificationsEventArgs> Notifications;

        private readonly List<string> warnings = new List<string>();

        internal void DummyOperation()
        {
            DoSomething();
            DoSomethingElse();

            Notifications?.Invoke(this, new NotificationsEventArgs(warnings));
        }

        private void DoSomething() { warnings.Add("First warning"); }
        private void DoSomethingElse() { warnings.Add("Second warning"); }


    }
}
